1. git clone <remote_repository_url>: Clone repository
2. git branch <branch_name>: Create a new branch on your current HEAD.
   + git branch -b <branch_name>: Create a new branch and switch HEAD branch.
   + git branch -d <branch_name>: Delete a local branch.	
3. git checkout <branch_name>: switch HEAD branch.
4. git add <file1> <file2> ...
5. git commit -m "Commit message": Commit previously staged changes
6. git fetch origin: Download all changes from but don’t integrate into HEAD:
7. git merge origin/main: Merge into your current HEAD
8. git push origin <branch_name>: Publish local changes on a remote.

*Note git fetch + git merge = git pull

9. git stash
    + Git stash save: Save the work in progress
	+ Git stash list
	+ Git stash apply <stash> : Roll back stashed changes
	+ Git stash pop <stash> : Xóa stash từ stack sau khi nó được apply nghĩa là mình đã lấy lại được các thay đổi
	+ Git stash show <stash>: Deleting the stash from the stack after it was applied means I got my changes back
	+ Git stash branch <name>
	+ Git stash clear
	+ Git stash drop
10. git reset <file_name> or git reset: Undo git add for uncommitted changes with

*Difference between “git add -A” and “git add”
 - Syntax: git add
   git add file_name.
   example: git add readme.md
 - Syntax: git add -a, git add -u
 
 https://www.geeksforgeeks.org/essential-git-commands/?ref=lbp